
//2
db.fruits.aggregate([
    { $unwind: "$origin" }
])

//2
db.fruits.aggregate([
    {
        $match: {onSale: true}},
        {$count: "fruitsOnSale"}
       
])
//3
db.fruits.aggregate([
    {$match: { stock: {$gt: 20 }  } },
    { $group: {_id: "$stock", totalStocks: {$sum: "$stock"} } },
    { $project: {_id: 0} }
])

//mam anu nga po difference neto dito:
/*
db.scores.aggregate([
    { $match: { stock: { $gt: 20 } } },
    { $count: "stockMoreThanTwenty" }
])// 0 fetch ito kaya alam kung mali,,hehe
*/
//4
db.fruits.aggregate(
   [
     {
       $group:
         {
           _id: "$item",
           avgAmount: { $avg: { $multiply: [ "$price", "$stock" ] } },
           avgQuantity: { $avg: "$stock" }
         }
     }
   ]
)

//5
db.fruits.aggregate(
   [
     {
       $group:
         {
           _id: "$item",
           maxTotalAmount: { $max: { $multiply: [ "$price", "$stock" ] } },
           maxQuantity: { $max: "$stock" }
         }
     }
   ]
)

